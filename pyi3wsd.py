#!/usr/bin/env python3.4

import socket
import json
import sys

from collections import defaultdict

from pyi3 import Event, Container, Connection, WindowEvent, get_socket_path


def get_mpv_property(prop_name):
    sock = socket.socket(socket.AF_UNIX)
    sockfile = sock.makefile()
    sock.connect('/tmp/mpvsocket')
    cmd = json.dumps({'command': ['get_property', prop_name]}) + '\n'
    sock.sendall(cmd.encode('utf8'))
    try:
        return json.loads(sockfile.readline().strip())['data']
    except KeyError:
        return ''


def urxvt(window: Container):
    title = window.window_properties.title.lower()
    if 'khaki' in title:
        return ''
    if 'vim' in title.lower():
        return ''
    return ''


def mpv(_: Container):
    path = get_mpv_property('path').lower()
    if 'youtube' in path:
        return ''
    if 'twitch.tv' in path:
        return ''
    return ''


SYMBOLS = defaultdict(lambda: '', {
    'chromium': '',
    'firefox': '',
    'google-chrome': '',
    'mpv': mpv,
    'rocket.chat': '',
    'st-256color': urxvt,
    'telegramdesktop': '',
    'urxvt': urxvt,
    'zathura': '',
})


def workspace_name(window: Container):
    if window.window_properties is None:
        return SYMBOLS['']

    mapped = SYMBOLS[window.window_properties.class_.lower()]

    if callable(mapped):
        return mapped(window)

    return mapped


def clean_current_workspace(c: Connection):
    tree = c.get_tree()
    wss = [ws for ws in c.get_workspaces() if ws.focused]
    for ws in wss:
        ws_cont, = tree.search_property(name=ws.name, type='workspace')
        if not ws_cont.nodes and not ws_cont.floating_nodes:
            c.command('rename workspace "{}" to "{}"'.format(ws_cont.name,
                                                             ws_cont.num))


def rename_by_window(c: Connection, changed: Container):
    ws = changed.workspace()
    if ws.name == '__i3_scratch':
        return
    new_name = "{}: {}".format(ws.num,
                               workspace_name(changed))
    c.command('rename workspace "{}" to "{}"'.format(ws.name, new_name))


def on_change(c: Connection, e: WindowEvent):
    changed, = c.get_tree().search_property(id=e.container.id)
    rename_by_window(c, changed)


def on_close(c: Connection, _: WindowEvent):
    clean_current_workspace(c)


def on_move(c: Connection, e: WindowEvent):
    clean_current_workspace(c)
    on_change(c, e)


def on_nudge(c: Connection):
    window, = c.get_tree().search_property(focused=True)
    rename_by_window(c, window)


def main():
    try:
        c = Connection()
    except:
        raise ValueError(get_socket_path())
    c.subscribe([Event.WINDOW, Event.MODE])

    while True:
        event = c.next_event()

        if event.change in ('focus', 'new'):
            on_change(c, event)

        if event.change == 'move':
            on_move(c, event)

        if event.change == 'close':
            on_close(c, event)

        if event.change == 'default':
            on_nudge(c)


if __name__ == '__main__':
    main()
